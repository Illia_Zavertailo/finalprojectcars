<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>User Management Application</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>

	<header>
		<nav class="navbar navbar-expand-md navbar-dark"
			style="background-color: tomato">
			<div>
				<a href="https://www.javaguides.net" class="navbar-brand"> Car Management App </a>
			</div>

			<ul class="navbar-nav">
				<li><a href="<%=request.getContextPath()%>/list"
					class="nav-link">Cars</a></li>
			</ul>
		</nav>
	</header>
	<br>
	<div class="container col-md-5">
		<div class="card">
			<div class="card-body">
				<c:if test="${car != null}">
					<form action="update" method="post">
				</c:if>
				<c:if test="${car == null}">
					<form action="insert" method="post">
				</c:if>

				<caption>
					<h2>
						<c:if test="${car != null}">
            			Edit User
            		</c:if>
						<c:if test="${car == null}">
            			Add New User
            		</c:if>
					</h2>
				</caption>

				<c:if test="${car != null}">
					<input type="hidden" name="id" value="<c:out value='${car.id}' />" />
				</c:if>

				<fieldset class="form-group">
					<label>Car Mark</label> <input type="text"
						value="<c:out value='${car.mark}' />" class="form-control"
						name="mark">
				</fieldset>

				<fieldset class="form-group">
					<label>Car Model</label> <input type="text"
						value="<c:out value='${car.model}' />" class="form-control"
						name="model">
				</fieldset>

				<fieldset class="form-group">
					<label>Car Class (1 to 3)</label> <input type="text"
						value="<c:out value='${car.classId}' />" class="form-control"
						name="classId">
				</fieldset>

				<fieldset class="form-group">
					<label>Car Power</label> <input type="text"
						value="<c:out value='${car.power}' />" class="form-control"
						name="power">
				</fieldset>

				<fieldset class="form-group">
					<label>Car Count of passengers</label> <input type="text"
						value="<c:out value='${car.passengers}' />" class="form-control"
						name="passengers">
				</fieldset>

				<fieldset class="form-group">
					<label>Car Description RU</label> <input type="text"
						value="<c:out value='${car.descriptionRU}' />" class="form-control"
						name="descriptionRU">
				</fieldset>

				<fieldset class="form-group">
					<label>Car Description EN</label> <input type="text"
						value="<c:out value='${car.descriptionEN}' />" class="form-control"
						name="descriptionEN">
				</fieldset>

				<fieldset class="form-group">
					<label>Car Picture Name</label> <input type="text"
						value="<c:out value='${car.classId}' />" class="form-control"
						name="classId">
				</fieldset>

				<fieldset class="form-group">
					<label>Car Price</label> <input type="text"
						value="<c:out value='${car.price}' />" class="form-control"
						name="price">
				</fieldset>

				<fieldset class="form-group">
					<label>Car Amount</label> <input type="text"
						value="<c:out value='${car.amount}' />" class="form-control"
						name="amount">
				</fieldset>

				<button type="submit" class="btn btn-success">Save</button>
				</form>
			</div>
		</div>
	</div>
</body>
</html>