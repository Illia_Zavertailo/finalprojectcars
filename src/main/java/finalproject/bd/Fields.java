package finalproject.bd;

/**
 * Holder for fields names of DB tables and beans.
 *
 * @author D.Kolesnikov
 */
public final class Fields {

    // entities
    public static final String ENTITY__ID = "id";
    public static final String ENTITY__CITY_ID = "id";

    public static final String USER__LOGIN = "username";
    public static final String USER__PASSWORD = "password";
    public static final String USER__FIRST_NAME = "name";
    public static final String USER__LAST_NAME = "surname";
    public static final String USER__EMAIL = "email";
    public static final String USER__PHONE = "phone";
    public static final String USER__ROLE_ID = "roleId";
    public static final String USER__DATE_OF_BIRTH = "birthDate";
    public static final String USER__PASSPORT_ID = "passportID";

    public static final String CAR__MARK = "mark";
    public static final String CAR__MODEL = "model";
    public static final String CAR__CLASS_ID = "classId";
    public static final String CAR__CITY_ID = "cityId";
    public static final String CAR__POWER = "power";
    public static final String CAR__DESCRIPTION_RU = "description_ru";
    public static final String CAR__DESCRIPTION_EN = "description_en";
    public static final String CAR__PICTURE = "picName";
    public static final String CAR__PASSENGERS = "passengers";
    public static final String CAR__PRICE = "price";
    public static final String CAR__AMOUNT = "amount";



    // beans
    public static final String USER_ORDER_BEAN__ORDER_ID = "id";
    public static final String USER_ORDER_BEAN__USER_FIRST_NAME = "first_name";
    public static final String USER_ORDER_BEAN__USER_LAST_NAME = "last_name";
    public static final String USER_ORDER_BEAN__ORDER_BILL = "bill";
    public static final String USER_ORDER_BEAN__STATUS_NAME = "name";


}