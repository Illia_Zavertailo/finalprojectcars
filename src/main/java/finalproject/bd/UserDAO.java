package finalproject.bd;

import finalproject.bd.entity.Role;
import finalproject.bd.entity.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDAO {

    public User create(int id) {
        User user = new User();
        Connection con = null;
        PreparedStatement prep;
        ResultSet rs;
        try {
            con = DBManager.getInstance().getConnection();
            prep = con.prepareStatement("SELECT * FROM usr WHERE id = '?'");
            prep.setInt(1, id);
            rs = prep.executeQuery();
            if (rs.next()) {
                UserMapper mapper = new UserMapper();
                user = mapper.mapRow(rs);
            }
        } catch (SQLException throwables) {
            assert con != null;
            DBManager.getInstance().rollbackAndClose(con);
            throwables.printStackTrace();
        } finally {
            assert con != null;
            DBManager.getInstance().close(con);
        }
        if (user.getFirstName() == null) {
            return null;
        }
        return user;
    }

    public User create(String username) {
        User user = new User();
        user.setUsername(username);
        Connection con = null;
        PreparedStatement prep;
        ResultSet rs;
        try {
            con = DBManager.getInstance().getConnection();
            prep = con.prepareStatement("SELECT * FROM usr WHERE username = ?");
            prep.setString(1, username);
            rs = prep.executeQuery();
            if (rs.next()) {
                UserMapper mapper = new UserMapper();
                user = mapper.mapRow(rs);
            }
        } catch (SQLException throwables) {
            DBManager.getInstance().rollbackAndClose(con);
            throwables.printStackTrace();
        } finally {
            assert con != null;
            DBManager.getInstance().close(con);
        }

        return user;
    }


    public void removeAtDB(String username) {
        Connection con = null;
        PreparedStatement prep;
        ResultSet rs;
        try {
            con = DBManager.getInstance().getConnection();
            prep = con.prepareStatement("DELETE * FROM usr WHERE username = ?");
            prep.setString(1, username);
            prep.executeUpdate();
        } catch (SQLException throwables) {
            DBManager.getInstance().rollbackAndClose(con);
            throwables.printStackTrace();
        } finally {
            assert con != null;
            DBManager.getInstance().close(con);
        }
    }

    public void removeAtDB(int id) {
        Connection con = null;
        PreparedStatement prep;
        ResultSet rs;
        try {
            con = DBManager.getInstance().getConnection();
            prep = con.prepareStatement("DELETE * FROM usr WHERE id = ?");
            prep.setInt(1, id);
            prep.executeUpdate();
        } catch (SQLException throwables) {
            DBManager.getInstance().rollbackAndClose(con);
            throwables.printStackTrace();
        } finally {
            assert con != null;
            DBManager.getInstance().close(con);
        }
    }

    public void addToDB(User user) {
        Connection con = null;
        PreparedStatement prep = null;
        ResultSet rs;
        try {
            con = DBManager.getInstance().getConnection();
            prep = con.prepareStatement("INSERT INTO cars_proj.usr (" +
                    Fields.USER__LOGIN + "," +
                    Fields.USER__PASSWORD + "," +
                    Fields.USER__DATE_OF_BIRTH + "," +
                    Fields.USER__ROLE_ID + "," +
                    Fields.USER__FIRST_NAME + "," +
                    Fields.USER__LAST_NAME + "," +
                    Fields.USER__EMAIL + "," +
                    Fields.USER__PHONE + "," +
                    Fields.USER__PASSPORT_ID + ") " +
                    "VALUES (?,?,?,?,?,?,?,?,?);");
            prep.setString(1, user.getUsername());
            prep.setString(2, user.getPassword());
            prep.setDate(3, user.getDateOfBirth());
            prep.setInt(4, user.getRoleId());
            prep.setString(5, user.getFirstName());
            prep.setString(6, user.getLastName());
            prep.setString(7, user.getEmail());
            prep.setString(8, user.getPhone());
            prep.setString(9, user.getPassportID());
            prep.executeUpdate();
        } catch (SQLException throwables) {
            DBManager.getInstance().rollbackAndClose(con);
            throwables.printStackTrace();
        } finally {
            assert con != null;
            DBManager.getInstance().close(con);
        }
    }

    private void update(User user) {
        Connection con = null;
        PreparedStatement prep;
        try {
            con = DBManager.getInstance().getConnection();
            prep = con.prepareStatement("UPDATE cars_proj.usr (" +
                    Fields.USER__LOGIN + "," +
                    Fields.USER__PASSWORD + "," +
                    Fields.USER__DATE_OF_BIRTH + "," +
                    Fields.USER__ROLE_ID + "," +
                    Fields.USER__FIRST_NAME + "," +
                    Fields.USER__LAST_NAME + "," +
                    Fields.USER__EMAIL + "," +
                    Fields.USER__PHONE + "," +
                    Fields.USER__PASSPORT_ID + ") " +
                    "VALUES (?,?,?,?,?,?,?,?,?) WHERE id = ?;");
            prep.setString(1, user.getUsername());
            prep.setString(2, user.getPassword());
            prep.setDate(3, user.getDateOfBirth());
            prep.setInt(4, user.getRoleId());
            prep.setString(5, user.getFirstName());
            prep.setString(6, user.getLastName());
            prep.setString(7, user.getEmail());
            prep.setString(8, user.getPhone());
            prep.setString(9, user.getPassportID());
            prep.setInt(10, user.getId());
            prep.executeUpdate();
        } catch (SQLException throwables) {
            DBManager.getInstance().rollbackAndClose(con);
            throwables.printStackTrace();
        } finally {
            assert con != null;
            DBManager.getInstance().close(con);
        }
    }

    public void changeRole(User user, Role role) {
        user.setRoleId(role.getId());
        update(user);
    }

    public boolean existsAtDB(String username) {
        User user = new UserDAO().create(username);
        return user.getFirstName() != null;
    }

    public boolean validate(User user, String password) {
        if (user.getPassword() != null) {
            return user.getPassword().equals(password);
        }
        return false;
    }

    public boolean existsAtDB(int id) {
        User user = new UserDAO().create(id);
        return user.getFirstName() != null;
    }

    public List<User> findAllUsers() {
        List<User> list = new ArrayList<>();
        Connection con = null;
        PreparedStatement prep;
        ResultSet rs;
        try {
            con = DBManager.getInstance().getConnection();
            prep = con.prepareStatement("SELECT * FROM usr");
            rs = prep.executeQuery();
            while (rs.next()) {
                UserDAO.UserMapper mapper = new UserDAO.UserMapper();
                list.add(mapper.mapRow(rs));
            }
        } catch (SQLException throwables) {
            assert con != null;
            DBManager.getInstance().rollbackAndClose(con);
            throwables.printStackTrace();
        } finally {
            assert con != null;
            DBManager.getInstance().close(con);
        }
        return list;
    }


    private static class UserMapper implements EntityMapper {

        @Override
        public User mapRow(ResultSet rs) {
            try {
                User user = new User();
                user.setId(rs.getInt(Fields.ENTITY__ID));
                user.setUsername(rs.getString(Fields.USER__LOGIN));
                user.setPassword(rs.getString(Fields.USER__PASSWORD));
                user.setFirstName(rs.getString(Fields.USER__FIRST_NAME));
                user.setLastName(rs.getString(Fields.USER__LAST_NAME));
                user.setRoleId(rs.getInt(Fields.USER__ROLE_ID));
                user.setDateOfBirth(rs.getDate(Fields.USER__DATE_OF_BIRTH));
                user.setPassportID(rs.getString(Fields.USER__PASSPORT_ID));
                user.setEmail(rs.getString(Fields.USER__EMAIL));
                user.setPhone(rs.getString(Fields.USER__PHONE));
                return user;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
