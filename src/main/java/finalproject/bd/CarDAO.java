package finalproject.bd;

import finalproject.bd.entity.Car;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CarDAO {

    public Car create(int id) {
        Car car = new Car();
        Connection con = null;
        PreparedStatement prep;
        ResultSet rs;
        try {
            con = DBManager.getInstance().getConnection();
            prep = con.prepareStatement("SELECT * FROM car WHERE id = '?'");
            prep.setInt(1, id);
            rs = prep.executeQuery();
            if (rs.next()) {
                CarMapper mapper = new CarMapper();
                car = mapper.mapRow(rs);
            }
        } catch (SQLException throwables) {
            assert con != null;
            DBManager.getInstance().rollbackAndClose(con);
            throwables.printStackTrace();
        } finally {
            assert con != null;
            DBManager.getInstance().close(con);
        }
        return car;
    }

    public Car create(String mark) {
        Car car = new Car();
        Connection con = null;
        PreparedStatement prep;
        ResultSet rs;
        try {
            con = DBManager.getInstance().getConnection();
            prep = con.prepareStatement("SELECT * FROM car WHERE mark = '?'");
            prep.setString(1, mark);
            rs = prep.executeQuery();
            if (rs.next()) {
                CarMapper mapper = new CarMapper();
                car = mapper.mapRow(rs);
            }
        } catch (SQLException throwables) {
            DBManager.getInstance().rollbackAndClose(con);
            throwables.printStackTrace();
        } finally {
            assert con != null;
            DBManager.getInstance().close(con);
        }

        return car;
    }


    public Car create(String mark, String model) {
        Car car = new Car();
        Connection con = null;
        PreparedStatement prep;
        ResultSet rs;
        try {
            con = DBManager.getInstance().getConnection();
            prep = con.prepareStatement("SELECT * FROM car WHERE mark = '?' AND model = '?';");
            prep.setString(1, mark);
            prep.setString(2, model);
            rs = prep.executeQuery();
            if (rs.next()) {
                CarMapper mapper = new CarMapper();
                car = mapper.mapRow(rs);
            }
        } catch (SQLException throwables) {
            DBManager.getInstance().rollbackAndClose(con);
            throwables.printStackTrace();
        } finally {
            assert con != null;
            DBManager.getInstance().close(con);
        }

        return car;
    }

    private void addCar(Car car) {
        Connection con = null;
        PreparedStatement prep = null;
        ResultSet rs;
        try {
            con = DBManager.getInstance().getConnection();
            prep = con.prepareStatement("INSERT INTO cars_proj.car (" +
                    Fields.CAR__MARK + "," +
                    Fields.CAR__MODEL + "," +
                    Fields.CAR__CLASS_ID + "," +
                    Fields.CAR__CITY_ID + "," +
                    Fields.CAR__POWER + "," +
                    Fields.CAR__PRICE + "," +
                    Fields.CAR__AMOUNT + "," +
                    Fields.CAR__DESCRIPTION_EN + "," +
                    Fields.CAR__DESCRIPTION_RU + "," +
                    Fields.CAR__PICTURE + "," +
                    Fields.CAR__PASSENGERS + ") " +
                    "VALUES (?,?,?,?,?,?,?,?,?,?,?);");
            prep.setString(1, car.getMark());
            prep.setString(2, car.getModel());
            prep.setInt(3, car.getClassId());
            prep.setInt(4, car.getCityId());
            prep.setInt(5, car.getPower());
            prep.setFloat(6, car.getPrice());
            prep.setInt(7, car.getAmount());
            prep.setString(8, car.getDescriptionEN());
            prep.setString(9, car.getDescriptionRU());
            prep.setString(10, car.getPictureName());
            prep.setInt(11, car.getPassengers());
            prep.executeUpdate();
        } catch (SQLException throwables) {
            assert con != null;
            DBManager.getInstance().rollbackAndClose(con);
            throwables.printStackTrace();
        } finally {
            assert con != null;
            DBManager.getInstance().close(con);
        }
    }

    public void updateCar(Car car) {
        Connection con = null;
        PreparedStatement prep = null;
        ResultSet rs;
        try {
            con = DBManager.getInstance().getConnection();
            prep = con.prepareStatement("UPDATE cars_proj.car (" +
                    Fields.CAR__MARK + "," +
                    Fields.CAR__MODEL + "," +
                    Fields.CAR__CLASS_ID + "," +
                    Fields.CAR__CITY_ID + "," +
                    Fields.CAR__POWER + "," +
                    Fields.CAR__PRICE + "," +
                    Fields.CAR__AMOUNT + "," +
                    Fields.CAR__DESCRIPTION_EN + "," +
                    Fields.CAR__DESCRIPTION_RU + "," +
                    Fields.CAR__PICTURE + "," +
                    Fields.CAR__PASSENGERS + ") " +
                    "VALUES (?,?,?,?,?,?,?,?,?,?,?) WHERE id=?;");
            prep.setString(1, car.getMark());
            prep.setString(2, car.getModel());
            prep.setInt(3, car.getClassId());
            prep.setInt(4, car.getCityId());
            prep.setInt(5, car.getPower());
            prep.setFloat(6, car.getPrice());
            prep.setInt(7, car.getAmount());
            prep.setString(8, car.getDescriptionEN());
            prep.setString(9, car.getDescriptionRU());
            prep.setString(10, car.getPictureName());
            prep.setInt(11, car.getPassengers());
            prep.setInt(12, car.getId());
            prep.executeUpdate();
        } catch (SQLException throwables) {
            assert con != null;
            DBManager.getInstance().rollbackAndClose(con);
            throwables.printStackTrace();
        } finally {
            assert con != null;
            DBManager.getInstance().close(con);
        }
    }

    public void addToDB(Car car) {
        Connection con = null;
        PreparedStatement prep;
        try {
            con = DBManager.getInstance().getConnection();
            prep = con.prepareStatement("INSERT INTO cars_proj.car (" +
                    Fields.CAR__MARK + "," +
                    Fields.CAR__MODEL + "," +
                    Fields.CAR__CLASS_ID + "," +
                    Fields.CAR__CITY_ID + "," +
                    Fields.CAR__POWER + "," +
                    Fields.CAR__PRICE + "," +
                    Fields.CAR__AMOUNT + "," +
                    Fields.CAR__DESCRIPTION_EN + "," +
                    Fields.CAR__DESCRIPTION_RU + "," +
                    Fields.CAR__PICTURE + "," +
                    Fields.CAR__PASSENGERS + ") " +
                    "VALUES (?,?,?,?,?,?,?,?,?,?,?);");
            prep.setString(1, car.getMark());
            prep.setString(2, car.getModel());
            prep.setInt(3, car.getClassId());
            prep.setInt(4, car.getCityId());
            prep.setInt(5, car.getPower());
            prep.setFloat(6, car.getPrice());
            prep.setInt(7, car.getAmount());
            prep.setString(8, car.getDescriptionEN());
            prep.setString(9, car.getDescriptionRU());
            prep.setString(10, car.getPictureName());
            prep.setInt(11, car.getPassengers());
            prep.executeUpdate();
        } catch (SQLException throwables) {
            DBManager.getInstance().rollbackAndClose(con);
            throwables.printStackTrace();
        } finally {
            assert con != null;
            DBManager.getInstance().close(con);
        }
    }

    public void deleteFromDB(Car car) {
        Connection con = null;
        PreparedStatement prep;
        try {
            con = DBManager.getInstance().getConnection();
            prep = con.prepareStatement("delete from users where id = ?;");
            prep.setInt(1, car.getId());
            prep.executeUpdate();
        } catch (SQLException throwables) {
            assert con != null;
            DBManager.getInstance().rollbackAndClose(con);
            throwables.printStackTrace();
        } finally {
            assert con != null;
            DBManager.getInstance().close(con);
        }
    }

    public void deleteFromDB(int id) {
        Connection con = null;
        PreparedStatement prep;
        try {
            con = DBManager.getInstance().getConnection();
            prep = con.prepareStatement("delete from users where id = ?;");
            prep.setInt(1, id);
            prep.executeUpdate();
        } catch (SQLException throwables) {
            assert con != null;
            DBManager.getInstance().rollbackAndClose(con);
            throwables.printStackTrace();
        } finally {
            assert con != null;
            DBManager.getInstance().close(con);
        }
    }

    public boolean existsAtDB(String username) {
        Car car = new CarDAO().create(username);
        return car.getMark() != null;
    }

    public boolean existsAtDB(int id) {
        Car car = new CarDAO().create(id);
        return car.getMark() != null;
    }

    public List<Car> findAllCars() {
        List<Car> list = new ArrayList<>();
        Connection con = null;
        PreparedStatement prep;
        ResultSet rs;
        try {
            con = DBManager.getInstance().getConnection();
            prep = con.prepareStatement("SELECT * FROM car");
            rs = prep.executeQuery();
            while (rs.next()) {
                CarDAO.CarMapper mapper = new CarDAO.CarMapper();
                list.add(mapper.mapRow(rs));
            }
        } catch (SQLException throwables) {
            assert con != null;
            DBManager.getInstance().rollbackAndClose(con);
            throwables.printStackTrace();
        } finally {
            assert con != null;
            DBManager.getInstance().close(con);
        }
        return list;
    }

    public List<Car> findByCity(int cityId) {
        List<Car> list = new ArrayList<>();
        Connection con = null;
        PreparedStatement prep;
        ResultSet rs;
        try {
            con = DBManager.getInstance().getConnection();
            prep = con.prepareStatement("SELECT * FROM usr WHERE cityId = ?");
            prep.setInt(1, cityId);
            rs = prep.executeQuery();
            while (rs.next()) {
                CarDAO.CarMapper mapper = new CarDAO.CarMapper();
                list.add(mapper.mapRow(rs));
            }
        } catch (SQLException throwables) {
            assert con != null;
            DBManager.getInstance().rollbackAndClose(con);
            throwables.printStackTrace();
        } finally {
            assert con != null;
            DBManager.getInstance().close(con);
        }
        return list;
    }

    public List<Car> findByClass(int classId) {
        List<Car> list = new ArrayList<>();
        Connection con = null;
        PreparedStatement prep;
        ResultSet rs;
        try {
            con = DBManager.getInstance().getConnection();
            prep = con.prepareStatement("SELECT * FROM usr WHERE classId = ?");
            prep.setInt(1, classId);
            rs = prep.executeQuery();
            while (rs.next()) {
                CarDAO.CarMapper mapper = new CarDAO.CarMapper();
                list.add(mapper.mapRow(rs));
            }
        } catch (SQLException throwables) {
            assert con != null;
            DBManager.getInstance().rollbackAndClose(con);
            throwables.printStackTrace();
        } finally {
            assert con != null;
            DBManager.getInstance().close(con);
        }
        return list;
    }


    private static class CarMapper implements EntityMapper {

        @Override
        public Car mapRow(ResultSet rs) {
            try {
                Car car = new Car();
                car.setId(rs.getInt(Fields.ENTITY__ID));
                car.setMark(rs.getString(Fields.CAR__MARK));
                car.setModel(rs.getString(Fields.CAR__MODEL));
                car.setCityId(rs.getInt(Fields.ENTITY__CITY_ID));
                car.setClassId(rs.getInt(Fields.CAR__CLASS_ID));
                car.setPower(rs.getInt(Fields.CAR__POWER));
                car.setPassengers(rs.getInt(Fields.CAR__PASSENGERS));
                car.setDescriptionRU(rs.getString(Fields.CAR__DESCRIPTION_RU));
                car.setDescriptionEN(rs.getString(Fields.CAR__DESCRIPTION_EN));
                car.setPictureName(rs.getString(Fields.CAR__PICTURE));
                car.setPrice(rs.getInt(Fields.CAR__PRICE));
                car.setAmount(rs.getInt(Fields.CAR__AMOUNT));
                return car;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
