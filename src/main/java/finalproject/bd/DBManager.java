package finalproject.bd;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.Logger;

public class DBManager {
    private static final String PROPERTIES_FILENAME = "app.properties";
    private static DBManager dbManager;
    private static final Logger log = Logger.getLogger(String.valueOf(DBManager.class));
    BasicDataSource connectionPool = new BasicDataSource();


    private DBManager() {
    }

    public static synchronized DBManager getInstance() {
        if (dbManager == null) {
            dbManager = new DBManager();
        }
        return dbManager;
    }

    public Connection getConnection() throws SQLException {

        this.connectionPool.setDriverClassName("com.mysql.cj.jdbc.Driver");
        this.connectionPool.setUrl(getConnectionUrl());
        this.connectionPool.setInitialSize(1);
        Connection con = null;
        //            Context initContext = new InitialContext();
//            Context envContext = (Context) initContext.lookup("java:/comp/env");
//            DataSource ds = (DataSource) envContext.lookup(connectionUrl);
//            con = ds.getConnection();
        con = this.connectionPool.getConnection();
        return con;
    }

    public void close(Connection con) {
        try {
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void rollbackAndClose(Connection con) {
        try {
            con.rollback();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private String getConnectionUrl() {
        FileInputStream fis;
        Properties property = new Properties();
        try {
            fis = new FileInputStream(PROPERTIES_FILENAME);
            property.load(fis);
            return property.getProperty("connection.url");
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return null;
    }

}
