package finalproject.bd.entity;

public enum Color {
    RED(1,"Красный","Red"),
    GREEN(2,"Зеленый","Green"),
    BLUE(3,"Синий","Blue"),
    YELLOW(4,"Желтый","Yellow"),
    BLACK(5,"Черный","Black"),
    SILVER(6,"Серебро","Silver"),
    WHITE(7,"Белый","White");

    int id;
    String nameRu;
    String nameEn;

    Color() {
    }

    Color(int id, String ru, String en) {
        this.id = id;
        this.nameRu = ru;
        this.nameEn = en;
    }
}
