package finalproject.bd.entity;

public enum Role {
    USER(1),
    MANAGER(2),
    ADMIN(3),
    ROOT(4);

    int id;

    Role(int id) {
        this.id = id;
    }

    Role() {
    }

    public static Role getRole(User user) {
        int roleId = user.getRoleId();
        return Role.values()[roleId];
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return name().toLowerCase();
    }
}
