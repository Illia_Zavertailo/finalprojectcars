package finalproject.bd.entity;

public enum CarClass {
    ECONOM(1,"Эконом","Econom"),
    BUSINESS(2,"Бизнес","Business"),
    LUXURY(3,"Люкс","Lux");

    int id;
    String nameRu;
    String nameEn;

    CarClass() {
    }

    CarClass(int id, String ru, String en) {
        this.id = id;
        this.nameRu = ru;
        this.nameEn = en;
    }
}
