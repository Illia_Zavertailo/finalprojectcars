package finalproject.bd.entity;

import java.sql.Date;

public class Order {


    private int id;
    private int userId;
    private int carId;
    private int statusId;
    private String returningCommentary;
    private String usrCommentary;
    private Date beginning;
    private Date end;


    public Order(int id) {
        this.id = id;
    }

    public Order(int userId, int carId) {
        this.userId = userId;
        this.carId = carId;
    }

    public Order() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public String getReturningCommentary() {
        return returningCommentary;
    }

    public void setReturningCommentary(String returningCommentary) {
        this.returningCommentary = returningCommentary;
    }

    public String getUsrCommentary() {
        return usrCommentary;
    }

    public void setUsrCommentary(String usrCommentary) {
        this.usrCommentary = usrCommentary;
    }

    public Date getBeginning() {
        return beginning;
    }

    public void setBeginning(Date beginning) {
        this.beginning = beginning;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Date getReturned() {
        return returned;
    }

    public void setReturned(Date returned) {
        this.returned = returned;
    }

    private Date returned;


}
