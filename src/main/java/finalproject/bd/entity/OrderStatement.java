package finalproject.bd.entity;

public enum OrderStatement {
    NOT_RETURNED(1,"Красный","Red"),
    RETURNED_WITH_PROBLEMS(2,"Красный","Red"),
    RETURNED_LATELY(3,"Красный","Red"),
    RETURNED_LATELY_WITH_PROBLEMS(5,"Красный","Red"),
    RETURNED(4,"Красный","Red");

    int id;
    String nameRu;
    String nameEn;

    OrderStatement() {
    }

    OrderStatement(int id, String ru, String en) {
        this.id = id;
        this.nameRu = ru;
        this.nameEn = en;
    }
}
