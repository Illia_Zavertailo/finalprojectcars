package finalproject.servlets;

import finalproject.bd.UserDAO;
import finalproject.bd.entity.User;
import java.io.IOException;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;


@WebServlet("/registration")
public class RegistrationServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(RegistrationServlet.class);
    private UserDAO userDAO = new UserDAO();

    public RegistrationServlet() {
        super();
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        log.trace("Registration starts");
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/views/registration.jsp");
        dispatcher.forward(req, resp);
        super.service(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/views/registration.jsp");
        dispatcher.forward(req, resp);
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String submitPassword = req.getParameter("verifyPassword");
        String email = req.getParameter("email");
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String phone = req.getParameter("phone");
        int day = Integer.parseInt(req.getParameter("day"));
        int month = Integer.parseInt(req.getParameter("month"))-1;
        int year = Integer.parseInt(req.getParameter("year"))-1900;

        if (!password.equals(submitPassword)) {
            RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/views/registration.jsp");
            dispatcher.forward(req, resp);
        }

        if (userDAO.existsAtDB(username)) {
            RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/views/erroruserexists.jsp");
            dispatcher.forward(req, resp);
        }
        User user = userDAO.create(username);
        user.setEmail(email);
        user.setPhone(phone);
        user.setPassword(password);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        Date date = new Date(year,month,day);
        user.setDateOfBirth(date);

        userDAO.addToDB(user);
        RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/views/userdetails.jsp");
        dispatcher.forward(req, resp);
//        resp.sendRedirect("/login");

    }

    @Override
    public void destroy() {
        log("Destroy");
    }

}
