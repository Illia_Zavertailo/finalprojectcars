package finalproject.servlets;

import finalproject.bd.UserDAO;
import finalproject.bd.entity.User;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private finalproject.bd.UserDAO UserDAO;

    public void init() {
        UserDAO = new UserDAO();
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/views/login.jsp");
        dispatcher.forward(req, resp);
        super.service(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/views/login.jsp");
        dispatcher.forward(req, resp);
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String errorMessage;
        User user = UserDAO.create(username);

        if (UserDAO.validate(user, password)) {
            response.sendRedirect("loginsuccess.jsp");
        } else {
            if (username == null || password == null || username.isEmpty() || password.isEmpty()) {
                errorMessage = "Login/password cannot be empty";
                request.setAttribute("errorMessage", errorMessage);
                response.sendRedirect("/login");
            }
                errorMessage = "Cannot find user with such login/password";
                request.setAttribute("errorMessage", errorMessage);

            response.sendRedirect("/login");
            HttpSession session = request.getSession();
            //session.setAttribute("user", username);
        }
    }
}
